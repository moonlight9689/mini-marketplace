<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1', 'namespace' => 'V1'], function(){

    Route::group(['middleware' => 'api', 'prefix' => 'auth'], function ($router) {
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');
    });
   
    Route::resource('authorization', 'AuthorizationController');
    
    Route::group(['prefix' => 'products'], function ($router) {
        Route::get('/', 'ProductsController@index')->name('products');
    });
});