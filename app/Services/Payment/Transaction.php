<?php
namespace App\Services\Payment;

use App\Models\Order;
use App\Models\Payment;
use App\Services\Basket\Basket;
use App\Services\Payment\Gateways\GatewayInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Transaction
{
    private $request;

    private $basket;


    public function __construct(Request $request, Basket $basket)
    {
        $this->request = $request;
        $this->basket = $basket;
    }

    
    public function checkout()
    {
        DB::beginTransaction();

        try {

            $order = $this->makeOrder();

            $payment = $this->makePayment($order);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return null;
        }

        if ($payment->isOnline()) {
            return $this->gatewayFactory()->pay($order);
        }

        $this->completeOrder($order);

        return $order;
    }


    public function verify()
    {
        $result = $this->gatewayFactory()->verify($this->request);

        if ($result['status'] === GatewayInterface::TRANSACTION_FAILED) return false;

        $this->confirmPayment($result);

        $this->completeOrder($result['order']);


        return true;
    }


    private function completeOrder($order)
    {

        $this->normalizeQuantity($order);

        $this->basket->clear();
    }





    private function normalizeQuantity($order)
    {
        foreach ($order->products as $product) {
            $product->decrementStock($product->pivot->quantity);
        }
    }




    private function confirmPayment($result)
    {
        return $result['order']->payment->confirm($result['refNum'], $result['gateway']);
    }

    private function gatewayFactory()
    {

        $gatewayPath = __NAMESPACE__ . '\Gateways\\' . ucfirst($this->request->geteway);

        if (!class_exists($gatewayPath)) {
            throw new \Exception("Gateway does not exist");
        }

        $gateway = new $gatewayPath();
        if (!is_subclass_of($gateway, GatewayInterface::class)) {
            throw new \Exception("class must implements \App\Services\Payment\Gateways\GatewayInterface");
        }

        return $gateway;
    }


    private function makeOrder()
    {
        $order = Order::create([
            'user_id' => auth()->user()->id,
            'code' => bin2hex(str_random(16)),
            'amount' => $this->basket->subTotal()
        ]);

        $order->products()->attach($this->basket->allProductQuantityPair());

        return $order;
    }


    private function makePayment($order)
    {
        return Payment::create([
            'order_id' => $order->id,
            'method' => $this->request->method,
            'amount' => $order->amount
        ]);
    }

}