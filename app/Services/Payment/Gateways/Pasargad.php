<?php
namespace App\Services\Payment\Gateways;

use App\Models\Order;
use Illuminate\Http\Request;

class Pasargad implements GatewayInterface
{
    public function pay(Order $order)
    {
        dd('Pasargad pay');
    }

    public function verify(Request $request)
    {
     # code   
    }

    public function getName():string
    {
        return 'pasargad';
    }
}