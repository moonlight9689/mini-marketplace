<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\Product\ProductCollection;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    
    const PAGINATE_COUNT = 10;

    public function index()
    {
        $products = Product::paginate(self::PAGINATE_COUNT);

        return responseApi('success', null, new ProductCollection($products));
    }
}
