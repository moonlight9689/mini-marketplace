<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Exceptions\QuantityExceededException;
use App\Models\Product;
use App\Services\Basket\Basket;
use App\Services\Payment\Transaction;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class BasketController extends Controller
{
    private $basket;
    private $transaction;

    public function __construct(Basket $basket, Transaction $transaction)
    {
        $this->middleware('auth')->only(['checkout']);
        $this->basket = $basket;
        $this->transaction = $transaction;
    }


    public function add(Product $product)
    {
        try {

            $this->basket->add($product, 1);

            return responseApi('success', __('payment added to basket'));
        
        } catch (QuantityExceededException $e) {
            throw ValidationException::withMessages([
                'quantity_exceeded' => [__('You can not add the product to basket because quantity exceeded.')],
            ]);
        }
    }


    public function index()
    {
        $items = $this->basket->all();

        return responseApi('success', null, $items);
    }


    public function update(Request $request, Product $product)
    {
        $this->basket->update($product, $request->quantity);
        
        return responseApi('success', __('your basket is updated'));
    }

    public function checkout(Request $request)
    {
        $this->validateForm($request);

        $order =  $this->transaction->checkout();

        return responseApi('success', __('your order has been registered', ['orderNum' => $order->id]));
    }



    private function validateForm($request)
    {
        $request->validate([
            'method' => ['required'],
            'gateway' => ['required_if:method,online']
        ]);
    }
}