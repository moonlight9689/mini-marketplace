<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Services\Payment\Transaction;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    private $transaction;

    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }



    public function verify(Request $request)
    {
        return  $this->transaction->verify()
            ? $this->sendSuccessResponse()
            : $this->sendErrorResponse();
    }

    private function sendErrorResponse()
    {
        return responseApi('error', __('Something went wrong, please try later'), null, 500);
    }

    private function sendSuccessResponse()
    {
        return responseApi('success', __('Your order successfully send.'));
    }
}
