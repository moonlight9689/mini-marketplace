<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\Authorization\PermissionCollection;
use App\Http\Resources\Authorization\RoleCollection;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;

class AuthorizationController extends Controller
{

    const PAGINATE_COUNT = 5;

    public function indexRole()
    {
        $roles = Role::paginate(self::PAGINATE_COUNT);

        return responseApi('success', null, new RoleCollection($roles));
    }
    
    public function storeRole(Request $request)
    {
        $this->validateRequest($request);

        Role::create($request->only('name', 'display_name'));

        return responseApi('success', __('role successfully created'));

    }

    public function updateRole(Request $request, Role $role)
    {
        $this->validateRequest($request);

        $role->update($request->only('display_name'));

        $role->refereshPermissions($request->permissions);

        return responseApi('success', __('role successfully updated'));

    }

    public function destroyRole(Role $role)
    {
        $role->delete();

        return responseApi('success', __('role successfully deleted'));
    }

    public function indexPermission()
    {
        $permissions = Permission::paginate(self::PAGINATE_COUNT);

        return responseApi('success', null, new PermissionCollection($permissions));
    }

    public function storePermission(Request $request)
    {
        $this->validateRequest($request);

        Permission::create($request->only('name', 'display_name'));

        return responseApi('success', __('permission successfully created'));
    }

    public function updatePermission(Request $request, Permission $permission)
    {
        $this->validateRequest($request);

        $permission->update($request->only('display_name'));

        return responseApi('success', __('role successfully updated'));
    }

    public function destroyPermission(Permission $permission)
    {
        $permission->delete();

        return responseApi('success', __('permission successfully deleted'));
    }
    private function validateRequest($request)
    {
        return $request->validate([
            'name' => ['require'],
            'display_name' => ['require']
        ]);
    }
}
