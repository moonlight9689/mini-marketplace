<?php
namespace App\Http\Resources\Authorization;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PermissionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => PermissionResource::collection($this->collection),
            'meta' => pagination($this)

        ];
    }
}